module modname {
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.base;

	requires org.apache.logging.log4j;
	requires javafx.graphics;
	requires chartfx.chart;
	requires chartfx.dataset;
	requires commons.math3;
	requires chartfx.math;
	requires org.controlsfx.controls;
	requires com.google.api.client.auth;
	requires com.google.api.client.extensions.java6.auth;
	requires google.api.client;
	requires com.google.api.client.extensions.jetty.auth;
	requires com.google.api.client;
	requires google.api.services.sheets.v4.rev614;
	requires jdk.httpserver;

	requires com.google.api.client.json.jackson2;
	requires com.google.api.client.json.gson;
	requires google.api.services.drive.v3.rev197;
	requires org.apache.commons.lang3;

	opens ca.tfuller.dfplot.gui;

}