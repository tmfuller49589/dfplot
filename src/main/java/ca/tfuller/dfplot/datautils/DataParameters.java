package ca.tfuller.dfplot.datautils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class DataParameters {
	protected static final Logger logger = LogManager.getLogger(DataParameters.class);

	private final BooleanProperty smoothProperty = new SimpleBooleanProperty();;
	private final DoubleProperty errorBarValueProperty = new SimpleDoubleProperty();
	private final IntegerProperty dataColumnProperty = new SimpleIntegerProperty();
	private final IntegerProperty smoothingWindowProperty = new SimpleIntegerProperty();
	private final ObjectProperty<ErrorBarValueTypeEnum> errorBarValueTypeProperty = new SimpleObjectProperty<>();

	private final ObjectProperty<Double> errorBarValuePropertyObject = errorBarValueProperty.asObject();
	private final ObjectProperty<Integer> smoothingWindowSizePropertyObject = smoothingWindowProperty.asObject();
	private final ObjectProperty<Integer> dataColumnPropertyObject = dataColumnProperty.asObject();

	public DataParameters() {
		dataColumnProperty.set(1);
		errorBarValueProperty.set(2);
		errorBarValueTypeProperty.set(ErrorBarValueTypeEnum.NONE);
		smoothProperty.set(false);
		smoothingWindowProperty.set(3);
	}

	public double getErrorBarValue() {
		return errorBarValueProperty.get();
	}

	public void setErrorBarValue(double errorBarValue) {
		this.errorBarValueProperty.set(errorBarValue);
	}

	public ErrorBarValueTypeEnum getErrorBarValueType() {
		return errorBarValueTypeProperty.get();
	}

	public void setErrorBarValueType(ErrorBarValueTypeEnum errorBarValueType) {
		this.errorBarValueTypeProperty.set(errorBarValueType);
	}

	public ObjectProperty<ErrorBarValueTypeEnum> getErrorBarValueTypeProperty() {
		return errorBarValueTypeProperty;
	}

	public int getDataColumn() {
		return dataColumnProperty.get();
	}

	public void setDataColumn(int dataColumn) {
		logger.info("setting data column to " + dataColumn);
		this.dataColumnProperty.set(dataColumn);
	}

	public boolean isSmooth() {
		return smoothProperty.get();
	}

	public void setSmooth(boolean smooth) {
		this.smoothProperty.set(smooth);
	}

	public int getSmoothingWindow() {
		return smoothingWindowProperty.get();
	}

	public void setSmoothingWindow(int smoothingWindow) {
		this.smoothingWindowProperty.set(smoothingWindow);
	}

	public IntegerProperty getDataColumnProperty() {
		return dataColumnProperty;
	}

	public BooleanProperty getSmoothProperty() {
		return smoothProperty;
	}

	public IntegerProperty getSmoothingWindowProperty() {
		return smoothingWindowProperty;
	}

	public DoubleProperty getErrorBarValueProperty() {
		return errorBarValueProperty;
	}

	public ObjectProperty<Integer> getSmoothingWindowSizePropertyObject() {
		return smoothingWindowSizePropertyObject;
	}

	public ObjectProperty<Integer> getDataColumnPropertyObject() {
		logger.info("data column property object " + dataColumnPropertyObject.hashCode());
		return dataColumnPropertyObject;
	}

	public ObjectProperty<Double> getErrorBarValuePropertyObject() {
		return errorBarValuePropertyObject;
	}

	public String getDescriptionString() {
		String s = "";
		s += "data column " + getDataColumn() + System.lineSeparator();
		s += "error bar value " + getErrorBarValue() + System.lineSeparator();

		return s;
	}

}
