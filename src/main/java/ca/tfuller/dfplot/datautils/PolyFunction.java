package ca.tfuller.dfplot.datautils;

import de.gsi.math.functions.AbstractFunction1D;

public class PolyFunction extends AbstractFunction1D {

	public static void main(String[] args0) {
		System.out.println(String.format("|% d|", 93));
		System.out.println(String.format("|% 6.3e|", 9.234e4));
		System.out.println(String.format("|% 6.3e|", -9.234e4));
	}

	public PolyFunction(final String name, final Double[] parameter) {
		super(name, new double[parameter.length]);
		// declare parameter names
		for (int i = 0; i < parameter.length; ++i) {
			this.setParameterName(i, Character.toString('a' + i));
		}

		// assign default values
		for (int i = 0; i < this.getParameterCount(); i++) {
			setParameterValue(i, parameter[i]);
		}
	}

	public PolyFunction(final String name, int degree) {
		super(name, new double[degree + 1]);
		// declare parameter names
		for (int i = 0; i <= degree; ++i) {
			this.setParameterName(i, Character.toString('a' + i));
		}

		// assign default values
		for (int i = 0; i < this.getParameterCount(); i++) {
			setParameterValue(i, 1);
		}
	}

	@Override
	public double getValue(final double x) {
		double val = 0.0;
		for (int i = 0; i < this.getParameterCount(); i++) {
			val += fparameter[i] * Math.pow(x, this.getParameterCount() - 1 - i);
		}

		return val;
	}

	public static String toString(double[] fittedParms) {
		//y = a x^3 + b x^2 + c x + d

		String s = "y = ";
		for (int i = 0; i < fittedParms.length; ++i) {
			if (i == fittedParms.length - 1) {
				s += String.format(" %+6.3e", fittedParms[i]);
			} else if (i == fittedParms.length - 2) {
				s += String.format(" %+6.3e x ", fittedParms[i]);
			} else {
				s += String.format(" %+6.3e x^%d", fittedParms[i], fittedParms.length - 1 - i);
			}
		}

		return s;
	}

}