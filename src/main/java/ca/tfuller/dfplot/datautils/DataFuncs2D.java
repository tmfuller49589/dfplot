package ca.tfuller.dfplot.datautils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.analysis.interpolation.LoessInterpolator;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataFuncs2D {

	protected static final Logger logger = LogManager.getLogger(DataFuncs2D.class);

	public static void main(String[] args) {
		slopeTest();
	}

	public double[] loessSmooth(double[] x, double[] y) {
		LoessInterpolator interpolator = new LoessInterpolator(LoessInterpolator.DEFAULT_BANDWIDTH,
				LoessInterpolator.DEFAULT_ROBUSTNESS_ITERS);
		double[] smooth = interpolator.smooth(x, y);
		return smooth;
	}

	public static double[][] slope(double[][] points, int width) {
		double[] x = new double[points.length];
		double[] y = new double[points.length];

		for (int i = 0; i < points.length; ++i) {
			x[i] = points[i][0];
			y[i] = points[i][1];
		}
		return slope(x, y, width);
	}

	public static double[][] slope(double[] x, double[] y, int width) {
		SimpleRegression regression = new SimpleRegression();

		double[][] slope = new double[x.length][];

		int offset = (int) Math.floor(width) / 2;
		List<Double> xvals = new ArrayList<Double>();

		for (int i = 0; i < x.length; ++i) {
			slope[i] = new double[2];

			int startidx = Math.max(0, i - offset);
			int endidx = Math.min(i + offset + 1, x.length);
			regression.clear();
			for (int j = startidx; j < endidx; ++j) {
				regression.addData(x[j], y[j]);
				xvals.add(x[j]);
			}

			slope[i][0] = DataFuncs1D.mean(xvals);
			slope[i][1] = regression.getSlope();
		}

		return slope;
	}

	public static void slopeTest() {
		int N = 20;
		double[] x = new double[N];
		double[] y = new double[N];
		System.out.println("data");
		for (int i = 0; i < N; ++i) {
			x[i] = i;
			y[i] = i * i + 3.4;
			System.out.println("(" + x[i] + ", " + y[i] + ")");
		}

		double[][] slope = slope(x, y, 3);

		System.out.println("slope");
		for (int i = 0; i < slope.length; ++i) {
			System.out.println("(" + slope[i][0] + ", " + slope[i][1] + ")");
		}

	}

	public static void linearRegressionTest() {

		SimpleRegression regression = new SimpleRegression();
		for (int i = 0; i < 10; ++i) {
			regression.addData(i, 2.0 * i + 3.0);
		}

		System.out.println("slope is " + regression.getSlope());
		System.out.println("intercept is " + regression.getIntercept());

	}

	/*
	 * x[rows][cols] contains x data for possibly several series
	 * y[rows][cols] contains y data for possibly several series
	 * 
	 *  Crop any rows if any x or y data is out of range.
	 *  If one series has an (x,y) out of range, omit the row for all series.
	 *  
	 */
	public static double[][] crop(int[] origDataColumnMap, double[][] x, double[][] y, double xmin, double xmax,
			double ymin, double ymax) {
		List<ArrayList<Double>> cx = new ArrayList<ArrayList<Double>>();
		List<ArrayList<Double>> cy = new ArrayList<ArrayList<Double>>();

		logger.info("xMin = " + xmin + " xMax = " + xmax + " yMin = " + ymin + " yMax = " + ymax);
		logger.info("size of x is " + x.length + " size of y is " + y.length);

		int xyCols = x[0].length + y[0].length;

		// loop over rows
		rowloop: for (int i = 0; i < x.length; ++i) {

			//loop over columns
			for (int j = 0; j < x[i].length; ++j) {
				logger.info(x[i][j] + " >= xmin is " + (x[i][j] >= xmin));
				logger.info(x[i][j] + " <= xmax is " + (x[i][j] <= xmax));
				logger.info(y[i][j] + " >= ymin is " + (y[i][j] >= ymin));
				logger.info(y[i][j] + " <= xmin is " + (y[i][j] <= ymax));

				if (x[i][j] < xmin || x[i][j] > xmax || y[i][j] < ymin || y[i][j] > ymax) {
					// data point is out of range,
					// move to next row
					continue rowloop;
				}
			}

			ArrayList<Double> cxRow = new ArrayList<Double>();
			ArrayList<Double> cyRow = new ArrayList<Double>();

			for (int j = 0; j < x[i].length; ++j) {
				cxRow.add(x[i][j]);
				cyRow.add(y[i][j]);
			}
			cx.add(cxRow);
			cy.add(cyRow);

		}

		// remap column order
		// if user selected x as column 2, and y as column 1,
		// we have to rearrange the data matrix

		double cropped[][] = new double[cx.size()][];
		for (int i = 0; i < cx.size(); ++i) {
			cropped[i] = new double[xyCols];
			for (int j = 0; j < origDataColumnMap.length; j += 2) {
				cropped[i][origDataColumnMap[j]] = cx.get(i).get(j);
				cropped[i][origDataColumnMap[j + 1]] = cy.get(i).get(j);
			}
		}

		return cropped;

	}

}
