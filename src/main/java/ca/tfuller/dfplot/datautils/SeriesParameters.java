package ca.tfuller.dfplot.datautils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.gsi.math.fitter.NonLinearRegressionFitter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SeriesParameters {
	private final StringProperty nameProperty = new SimpleStringProperty();
	private final DataParameters[] dataParameters = new DataParameters[2]; // for x and y values
	private final ObjectProperty<FitTypeEnum> fitTypeProperty = new SimpleObjectProperty<>();
	private final IntegerProperty fitDegreeProperty = new SimpleIntegerProperty();
	private final DoubleProperty dxSizeProperty = new SimpleDoubleProperty();
	private final ObjectProperty<PlotTypeEnum> plotTypeProperty = new SimpleObjectProperty<>();
	private final SeriesRenderer dataRenderer;
	private final SeriesRenderer fitRenderer;
	private NonLinearRegressionFitter fitter;

	private final ObjectProperty<Double> deltaXSizePropertyObject = dxSizeProperty.asObject();
	private final ObjectProperty<Integer> fitDegreePropertyObject = fitDegreeProperty.asObject();

	protected static final Logger logger = LogManager.getLogger(SeriesParameters.class);

	public SeriesParameters(String name) {
		this.nameProperty.set(name);
		dataParameters[0] = new DataParameters();
		dataParameters[1] = new DataParameters();
		fitTypeProperty.set(FitTypeEnum.NONE);
		fitDegreeProperty.set(1);
		dxSizeProperty.set(5);
		plotTypeProperty.set(PlotTypeEnum.DATA);

		dataRenderer = new SeriesRenderer(name);
		fitRenderer = new SeriesRenderer(name + " fit");
	}

	public String getName() {
		return nameProperty.get();
	}

	public NonLinearRegressionFitter getFitter() {
		return fitter;
	}

	public void setFitter(NonLinearRegressionFitter fitter) {
		this.fitter = fitter;
	}

	public void setName(String name) {
		this.nameProperty.set(name);
		dataRenderer.setName(name);
		fitRenderer.setName(name + " fit");
	}

	public DataParameters getxDataParameters() {
		return dataParameters[0];
	}

	public void setxDataParameters(DataParameters xDataParameters) {
		this.dataParameters[0] = xDataParameters;
	}

	public DataParameters getyDataParameters() {
		return dataParameters[1];
	}

	public void setyDataParameters(DataParameters yDataParameters) {
		this.dataParameters[1] = yDataParameters;
	}

	public FitTypeEnum getFitType() {
		return fitTypeProperty.get();
	}

	public ObjectProperty<FitTypeEnum> getFitTypeProperty() {
		return fitTypeProperty;
	}

	public int getFitDegree() {
		return fitDegreeProperty.get();
	}

	public void setFitDegree(int fitDegree) {
		this.fitDegreeProperty.set(fitDegree);
	}

	public double getDxSize() {
		return dxSizeProperty.get();
	}

	public void setDxSize(double dxSize) {
		this.dxSizeProperty.set(dxSize);
	}

	public void setPlotType(PlotTypeEnum plotType) {
		this.plotTypeProperty.set(plotType);
	}

	public ObjectProperty<PlotTypeEnum> getPlotTypeProperty() {
		return plotTypeProperty;
	}

	public PlotTypeEnum getPlotType() {
		return plotTypeProperty.get();
	}

	public StringProperty getNameProperty() {
		return nameProperty;
	}

	public IntegerProperty getFitDegreeProperty() {
		return fitDegreeProperty;
	}

	public DoubleProperty getDxSizeProperty() {
		return dxSizeProperty;
	}

	@Override
	public String toString() {
		return nameProperty.get();
	}

	public ObjectProperty<Double> getDeltaXSizePropertyObject() {
		return deltaXSizePropertyObject;
	}

	public ObjectProperty<Integer> getFitDegreePropertyObject() {
		return fitDegreePropertyObject;
	}

	public SeriesRenderer getDataRenderer() {
		return dataRenderer;
	}

	public SeriesRenderer getFitRenderer() {
		return fitRenderer;
	}

	public String getDescriptionString() {
		String s = System.lineSeparator();
		s += "id: " + this.hashCode() + System.lineSeparator();
		s += nameProperty.get() + System.lineSeparator();
		s += "x axis: " + System.lineSeparator();
		s += dataParameters[0].getDescriptionString() + System.lineSeparator();
		s += "y axis: " + System.lineSeparator();
		s += dataParameters[1].getDescriptionString() + System.lineSeparator();
		s += "fit type: " + fitTypeProperty.get().toString() + System.lineSeparator();
		s += "fit degree: " + fitDegreeProperty.get() + System.lineSeparator();
		s += "dx size: " + dxSizeProperty.get() + System.lineSeparator();
		s += "plot type: " + plotTypeProperty + System.lineSeparator();
		s += dataRenderer.getDescriptionString();
		return s;
	}

	public String getFitString() {
		String fitString = "";

		double[] fittedParms = fitter.getBestEstimates();
		switch (fitTypeProperty.getValue()) {
		case EXPONENTIAL:
			fitString = ExponentialFunction.toString(fittedParms);
			break;
		case NONE:
			break;
		case POLYNOMIAL:
			fitString = PolyFunction.toString(fittedParms);
			break;
		case SINE:
			fitString = SineFunction.toString(fittedParms);
			break;
		default:
			break;

		}
		return fitString;
	}

	public String getFitStatsString() {
		double[] fittedParameter = fitter.getBestEstimates();
		double[] fittedParameterError = fitter.getBestEstimatesErrors();

		String s = "";
		for (int i = 0; i < fittedParameter.length; ++i) {
			s += String.format("fitted parameter: % 6.3e  error estimate: % 6.3e\n", fittedParameter[i],
					fittedParameterError[i]);

		}
		System.out.println(s);
		return s;

	}
}
