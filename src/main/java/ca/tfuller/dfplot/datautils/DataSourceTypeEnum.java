package ca.tfuller.dfplot.datautils;

public enum DataSourceTypeEnum {
	GOOGLESHEET, LOCALCSV, GENERATED;
}
