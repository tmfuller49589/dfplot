package ca.tfuller.dfplot.datautils;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataFuncs1D {

	protected static final Logger logger = LogManager.getLogger(DataFuncs1D.class);
	private DataSource dataSource;

	public DataFuncs1D(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataReader) {
		this.dataSource = dataReader;
	}

	/*
	 * errValue can be a column index, an absolute value, or a percent
	 * column is the column that contains the data (not the error values)
	 */
	public double[] assignError(ErrorBarValueTypeEnum errorType, double[] val, Double errValue, int colIdx) {
		double err[] = null;

		switch (errorType) {
		case COLUMN:
			err = dataSource.getColumnAsPrimitive(errValue.intValue());
			break;
		case ABSOLUTE:
			err = new double[val.length];
			for (int i = 0; i < dataSource.getRowCount(colIdx); ++i) {
				err[i] = errValue;
			}
			break;
		case NONE:
			err = new double[val.length];
			for (int i = 0; i < val.length; ++i) {
				err[i] = 0.0;
			}
			break;
		case PERCENT:
			err = new double[val.length];
			for (int i = 0; i < val.length; ++i) {
				err[i] = Math.abs(val[i]) * errValue / 100.0;
			}
			break;
		}
		return err;
	}

	public static void scale(double[] val, double scaleFactor) {
		if (scaleFactor == 1.0d) {
			return;
		}
		for (int i = 0; i < val.length; ++i) {
			val[i] *= scaleFactor;
		}
	}

	public static void shiftToZero(boolean shift, double[] val) {
		if (!shift) {
			return;
		}
		double init = val[0];
		for (int i = 0; i < val.length; ++i) {
			val[i] -= init;
		}
	}

	public static void boxcar(Boolean smooth, double[] x, int width) {
		if (!smooth) {
			return;
		}

		double[] smoothed = new double[x.length];
		int offset = (int) Math.floor(width) / 2;

		for (int i = 0; i < x.length; ++i) {
			int startidx = Math.max(0, i - offset);
			int endidx = Math.min(i + offset + 1, x.length);
			double sum = 0d;
			for (int j = startidx; j < endidx; ++j) {
				sum += x[j];
			}

			int N = endidx - startidx;
			smoothed[i] = sum / (N);
		}

		for (int i = 0; i < x.length; ++i) {
			x[i] = smoothed[i];
		}
	}

	public static void smoothTest() {
		int N = 10;
		double[] x = new double[N];
		for (int i = 0; i < N; ++i) {
			x[i] = i;
		}

		System.out.println("original");
		for (double d : x) {
			System.out.println(d);
		}

		boxcar(true, x, 5);

		System.out.println("smoothed");
		for (double d : x) {
			System.out.println(d);
		}
	}

	public static double mean(List<Double> xvals) {
		double sum = 0;
		for (Double x : xvals) {
			sum += x;
		}
		double mean = sum / xvals.size();
		return mean;
	}

	public static double mean(double[] x) {
		double sum = 0;
		for (double x1 : x) {
			sum += x1;
		}
		double mean = sum / x.length;
		return mean;
	}

	public static double max(double[] x) {
		double max = Double.MIN_NORMAL;
		for (double x1 : x) {
			max = Math.max(x1, max);
		}
		return max;
	}

	public static double min(double[] x) {
		double min = Double.MIN_NORMAL;
		for (double x1 : x) {
			min = Math.min(x1, min);
		}
		return min;
	}
}
