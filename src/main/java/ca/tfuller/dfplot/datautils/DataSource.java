package ca.tfuller.dfplot.datautils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class DataSource {

	protected static final Logger logger = LogManager.getLogger(DataSource.class);

	protected List<ArrayList<Double>> columns = new ArrayList<ArrayList<Double>>();
	protected ArrayList<String> stringValues;

	protected String filename;
	protected String nameForChartTitle;

	private static DataSourceTypeEnum dataSourceType = null;

	public static DataSourceTypeEnum getDataSourceType() {
		return dataSourceType;
	}

	public static void setDataSourceType(DataSourceTypeEnum dataSourceType) {
		DataSource.dataSourceType = dataSourceType;
	}

	public List<ArrayList<Double>> getColumns() {
		return columns;
	}

	public void print() {

		int maxRows = Integer.MIN_VALUE;
		for (int i = 0; i < columns.size(); ++i) {
			maxRows = Math.max(maxRows, columns.get(i).size());
		}

		for (int row = 0; row < maxRows; ++row) {
			String s = "";
			for (int col = 0; col < columns.size(); ++col) {
				if (row < columns.get(col).size()) {
					s += String.format("%10.3f,", columns.get(col).get(row));
				} else {
					s += String.format("%10s,", "");
				}

			}
			System.out.println(s);
		}
	}

	public void crop(List<SeriesParameters> seriesParametersList, double xmin, double xmax, double ymin, double ymax) {

		for (SeriesParameters sp : seriesParametersList) {

			DataParameters xDataParameters = sp.getxDataParameters();
			DataParameters yDataParameters = sp.getyDataParameters();

			Iterator<Double> itx = getColumn(xDataParameters.getDataColumn() - 1).iterator();
			Iterator<Double> ity = getColumn(yDataParameters.getDataColumn() - 1).iterator();
			while (itx.hasNext()) {
				Double x = itx.next();
				Double y = ity.next();
				if (x < xmin || x > xmax || y < ymin || y > ymax) {
					itx.remove();
					ity.remove();
				}
			}
		}
	}

	public List<Double> getColumn(int colIdx) {
		return columns.get(colIdx);
	}

	public double[] getColumnAsPrimitive(int colIdx) {
		ArrayList<Double> col = columns.get(colIdx);
		Double[] x = new Double[col.size()];
		return ArrayUtils.toPrimitive(col.toArray(x));
	}

	public ArrayList<String> getStringValues() {
		return stringValues;
	}

	public void setStringValues(ArrayList<String> stringValues) {
		this.stringValues = stringValues;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public int getRowCount(int colIdx) {
		return columns.get(colIdx).size();
	}

	public int getMaxRowCount() {
		int mrc = Integer.MIN_VALUE;
		for (ArrayList<Double> col : columns) {
			mrc = Math.max(mrc, col.size());
		}
		return mrc;
	}

	public int getColumnCount() {
		return columns.size();
	}

	public abstract String getNameForChartTitle();

}
