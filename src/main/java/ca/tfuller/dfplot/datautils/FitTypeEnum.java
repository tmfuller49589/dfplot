package ca.tfuller.dfplot.datautils;

public enum FitTypeEnum {
	NONE("none"), POLYNOMIAL("polynomial"), SINE("sine"), EXPONENTIAL("exponential");

	private String name;

	FitTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

}
