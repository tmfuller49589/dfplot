package ca.tfuller.dfplot.datautils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataReaderCsv extends DataSource implements DataReaderInterface {

	protected static final Logger logger = LogManager.getLogger(DataReaderCsv.class);

	public DataReaderCsv(String filename, int firstDataRow) {
		this.filename = filename;
		nameForChartTitle = filename;
		setDataSourceType(DataSourceTypeEnum.LOCALCSV);
		read(firstDataRow);
		parse();
		print();
	}

	private void parse() {

		// initialize the matrix
		int rows = stringValues.size();
		columns.clear();

		String rowString = stringValues.get(0);
		String[] tokens = rowString.split(",");
		int cols = tokens.length;

		for (int i = 0; i < cols; ++i) {
			columns.add(new ArrayList<Double>());
		}

		for (int row = 0; row < rows; ++row) {
			rowString = stringValues.get(row);
			logger.info(rowString);
			tokens = rowString.split(",");
			if (tokens.length != cols) {
				logger.info("parse error on line " + row + " expected " + cols + " columns but found " + tokens.length);
				logger.info("offending line -->" + rowString + "<--");
			}

			int col = 0;
			for (String token : tokens) {
				try {
					columns.get(col).add(Double.parseDouble(token));
				} catch (NumberFormatException e) {
					logger.info("parse error, could not convert -->" + token + "<-- to double");
				} finally {
					++col;
				}
			}
		}
	}

	@Override
	public void read(int firstDataRow) {

		// read datafile into a string array
		stringValues = new ArrayList<String>();

		int row = 1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (row >= firstDataRow) {
					stringValues.add(line);
				}
				++row;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getNameForChartTitle() {
		return nameForChartTitle;
	}

}
