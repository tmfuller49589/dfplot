package ca.tfuller.dfplot.datautils;

public enum PlotTypeEnum {

	DATA("data"), FIRST_DERIVATIVE("first derivative"), SECOND_DERIVATIVE("second derivative");

	private String name;

	PlotTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

}
