package ca.tfuller.dfplot.datautils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.gsi.chart.marker.DefaultMarker;
import de.gsi.chart.marker.Marker;
import de.gsi.chart.renderer.ErrorStyle;
import de.gsi.chart.renderer.LineStyle;
import de.gsi.chart.renderer.spi.ErrorDataSetRenderer;
import de.gsi.dataset.spi.DoubleErrorDataSet;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SeriesRenderer {
	private final StringProperty cssStyleProperty = new SimpleStringProperty();
	private Marker marker = DefaultMarker.CIRCLE;
	private final ErrorDataSetRenderer errorRenderer = new ErrorDataSetRenderer();

	private final ObjectProperty<Double> markerSizePropertyObject = errorRenderer.markerSizeProperty().asObject();
	private final DoubleErrorDataSet dataSet = new DoubleErrorDataSet("series name");

	protected static final Logger logger = LogManager.getLogger(SeriesRenderer.class);

	public SeriesRenderer(String name) {
		cssStyleProperty.set("markerColor=red;strokeColor=blue;");
		dataSet.setStyle(cssStyleProperty.get());

		errorRenderer.setMarkerSize(2.0);
		errorRenderer.setDrawMarker(true);
		errorRenderer.setErrorType(ErrorStyle.NONE);
		errorRenderer.setPolyLineStyle(LineStyle.NORMAL);
		errorRenderer.setPointReduction(false);

		dataSet.setName(name);

		errorRenderer.getDatasets().clear();
		errorRenderer.getDatasets().setAll(dataSet);

	}

	public String getName() {
		return dataSet.getName();
	}

	public void setName(String name) {
		dataSet.setName(name);
		logger.info("setting name to " + dataSet.getName());
	}

	public String getCssStyle() {
		return cssStyleProperty.get();
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyleProperty.set(cssStyle);
		dataSet.setStyle(cssStyleProperty.get());
	}

	public ErrorDataSetRenderer getErrorRenderer() {
		return errorRenderer;
	}

	public StringProperty getCssStyleProperty() {
		return cssStyleProperty;
	}

	public ObjectProperty<Double> getMarkerSizePropertyObject() {
		return markerSizePropertyObject;
	}

	public DoubleErrorDataSet getDataSet() {
		return dataSet;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
		errorRenderer.setMarker(marker);
	}

	public String getDescriptionString() {
		String s = System.lineSeparator();
		s += "id: " + this.hashCode() + System.lineSeparator();
		s += "x axis: " + System.lineSeparator();
		s += "y axis: " + System.lineSeparator();
		s += "css style: " + System.lineSeparator();
		s += "marker size: " + errorRenderer.getMarkerSize() + System.lineSeparator();
		s += "marker " + getMarker() + System.lineSeparator();
		s += "display markers: " + errorRenderer.isDrawMarker() + System.lineSeparator();
		s += "error style: " + errorRenderer.getErrorType() + System.lineSeparator();
		s += "line style: " + errorRenderer.getPolyLineStyle() + System.lineSeparator();
		return s;
	}
}
