package ca.tfuller.dfplot.datautils;

public interface DataReaderInterface {

	public void read(int firstRow);

}
