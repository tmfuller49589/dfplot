package ca.tfuller.dfplot.datautils;

import de.gsi.math.functions.AbstractFunction1D;

/*
 * y = a sin(b * x + c) + d
 */
public class SineFunction extends AbstractFunction1D {

	public SineFunction(final String name, final Double[] parameter) {
		super(name, new double[parameter.length]);
		// declare parameter names
		for (int i = 0; i < parameter.length; ++i) {
			this.setParameterName(i, Character.toString('a' + i));
		}

		// assign default values
		for (int i = 0; i < this.getParameterCount(); i++) {
			setParameterValue(i, parameter[i]);
		}
	}

	public SineFunction() {
		super("y = a sin(b (x + c)) + d", new double[4]);
		// declare parameter names
		for (int i = 0; i < 4; ++i) {
			this.setParameterName(i, Character.toString('a' + i));
		}

		// assign default values
		for (int i = 0; i < this.getParameterCount(); i++) {
			setParameterValue(i, 1);
		}
	}

	@Override
	public double getValue(final double x) {
		double val = 0.0;
		for (int i = 0; i < this.getParameterCount(); i++) {
			val += fparameter[0] * Math.sin(fparameter[1] * (x - fparameter[2])) + fparameter[3];
		}

		return val;
	}

	public static String toString(double[] fittedParms) {
		//y = a sin(b (x - c)) + d
		String s = String.format("y = % 5.3e sin(% 5.3e (x%+5.3e)) %+5.3e", fittedParms[0], fittedParms[1],
				-fittedParms[2], fittedParms[3]);
		return s;
	}

}