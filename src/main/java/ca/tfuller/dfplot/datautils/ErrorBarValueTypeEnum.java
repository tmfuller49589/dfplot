package ca.tfuller.dfplot.datautils;

public enum ErrorBarValueTypeEnum {

	NONE("none"), ABSOLUTE("absolute"), PERCENT("percent"), COLUMN("data column");

	private String name;

	ErrorBarValueTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

}
