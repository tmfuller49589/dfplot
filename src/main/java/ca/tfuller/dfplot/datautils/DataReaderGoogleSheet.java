package ca.tfuller.dfplot.datautils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.ValueRange;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class DataReaderGoogleSheet extends DataSource implements DataReaderInterface {
	private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
	private static final JsonFactory GSON_FACTORY = GsonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	/** Directory to store user credentials for this application. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"),
			".credentials/dfplot");
	/**
	 * Global instance of the scopes required by this quickstart. If modifying
	 * these scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "credentials.json";

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;

	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	private static int formattingExceptions = 0;

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public DataReaderGoogleSheet(String url, int firstDataRow) {
		this.filename = url;
		setDataSourceType(DataSourceTypeEnum.GOOGLESHEET);
		read(firstDataRow);
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT
	 *            The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException
	 *             If the credentials.json file cannot be found.
	 */
	private static Credential authorize() throws IOException {
		// Load client secrets.
		InputStream in = DataReaderGoogleSheet.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(GSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, GSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	public String getFileIdFromUrl(String url) {
		String fileId = url.split("/d/")[1];
		fileId = fileId.split("/edit")[0];
		return fileId;
	}

	@Override
	public void read(int firstDataRow) {
		columns.clear();

		List<List<Object>> values = importSheet();

		// get rid of the first non data rows
		Iterator<List<Object>> rowIt = values.iterator();
		for (int i = 1; i < firstDataRow; ++i) {
			rowIt.next();
			rowIt.remove();
		}

		// find max number of columns:
		int maxCols = Integer.MIN_VALUE;
		for (List<Object> row : values) {
			maxCols = Math.max(maxCols, row.size());
		}

		for (int i = 0; i < maxCols; ++i) {
			columns.add(new ArrayList<Double>());
		}
		formattingExceptions = 0;

		for (List<Object> row : values) {
			int colIdx = 0;

			for (Object o : row) {
				try {
					columns.get(colIdx).add(Double.parseDouble(o.toString()));
					logger.info(o.toString());
				} catch (NumberFormatException e) {
					if (!(o.toString().isBlank() || o.toString().isEmpty())) {
						++formattingExceptions;
						logger.info("caught number format exception for input -->" + o.toString() + "<-- "
								+ formattingExceptions);
					}
					columns.get(colIdx).add(Double.NaN);
				}
				++colIdx;
			}
		}

		/*
		 * Remove NaN from bottoms of columns
		 */
		for (ArrayList<Double> col : columns) {
			ListIterator<Double> it = col.listIterator(col.size());
			while (it.hasPrevious()) {
				if (it.previous().isNaN()) {
					it.remove();
				}
			}
		}

		logger.info("there were " + formattingExceptions + " format exceptions");

		if (formattingExceptions > 0) {
			Platform.runLater(() -> {
				//an event with a button maybe
				System.out.println("button is clicked");
				Alert a = new Alert(AlertType.WARNING);

				String s = "There were " + formattingExceptions + " cases where sheet values could not";
				s += " be converted to numbers.";
				s += " Check that the first data row is set correctly,";
				s += " and that the spreadsheet contains only numeric data at and below";
				s += " the first data row.";
				logger.info(s);
				a.setContentText(s);
				a.showAndWait();
			});

		}
	}

	public List<List<Object>> importSheet() {
		List<List<Object>> values = null;
		try {
			final String range = "A:Z";
			Credential credential = authorize();
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, GSON_FACTORY, credential)
					.setApplicationName(APPLICATION_NAME).build();

			String spreadsheetId = getFileIdFromUrl(filename);

			Spreadsheet ss = service.spreadsheets().get(spreadsheetId).execute();
			nameForChartTitle = ss.getProperties().getTitle();

			ValueRange response;
			response = service.spreadsheets().values().get(spreadsheetId, range).execute();

			values = response.getValues();
			if (values == null || values.isEmpty()) {
				logger.error("No data found.");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Platform.runLater(() -> {
				Alert a = new Alert(AlertType.ERROR);
				a.setContentText("There was an IO exception while trying to read the sheet");
				a.showAndWait();
			});
		}
		return values;
	}

	@Override
	public String getNameForChartTitle() {
		return nameForChartTitle;
	}
}