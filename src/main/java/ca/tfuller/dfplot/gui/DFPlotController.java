package ca.tfuller.dfplot.gui;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfplot.datautils.DataFuncs1D;
import ca.tfuller.dfplot.datautils.DataFuncs2D;
import ca.tfuller.dfplot.datautils.DataReaderCsv;
import ca.tfuller.dfplot.datautils.DataReaderGoogleSheet;
import ca.tfuller.dfplot.datautils.DataSource;
import ca.tfuller.dfplot.datautils.ErrorBarValueTypeEnum;
import ca.tfuller.dfplot.datautils.ExponentialFunction;
import ca.tfuller.dfplot.datautils.FitTypeEnum;
import ca.tfuller.dfplot.datautils.PlotTypeEnum;
import ca.tfuller.dfplot.datautils.PolyFunction;
import ca.tfuller.dfplot.datautils.SeriesParameters;
import ca.tfuller.dfplot.datautils.SeriesRenderer;
import ca.tfuller.dfplot.datautils.SineFunction;
import de.gsi.chart.XYChart;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.marker.DefaultMarker;
import de.gsi.chart.marker.Marker;
import de.gsi.chart.plugins.DataPointTooltip;
import de.gsi.chart.plugins.EditAxis;
import de.gsi.chart.plugins.EditDataSet;
import de.gsi.chart.plugins.Panner;
import de.gsi.chart.plugins.TableViewer;
import de.gsi.chart.plugins.Zoomer;
import de.gsi.chart.renderer.ErrorStyle;
import de.gsi.chart.renderer.LineStyle;
import de.gsi.chart.renderer.Renderer;
import de.gsi.chart.renderer.spi.ErrorDataSetRenderer;
import de.gsi.dataset.DataSet;
import de.gsi.dataset.spi.DoubleDataSet;
import de.gsi.dataset.spi.DoubleErrorDataSet;
import de.gsi.math.fitter.NonLinearRegressionFitter;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;

public class DFPlotController {
	// TODO try css style
	// TODO series rename in combo box
	// TODO pdf generation?

	protected static final Logger logger = LogManager.getLogger(DFPlotController.class);

	private final ObservableList<SeriesParameters> seriesParametersList = FXCollections
			.observableArrayList(p -> new Observable[] { p.getNameProperty() });

	private ChangeListener<FitTypeEnum> fitTypeChangeListener;
	private Stage fitStatsStage = null;
	private DFFitStatsController fitStatsController = null;

	private Stage primaryStage;

	private XYChart chart;
	private DataSource dataSource;
	private DataFuncs1D dataFuncs1D;
	private Stage plotWindow = null;

	private DefaultNumericAxis xAxis;
	private DefaultNumericAxis yAxis;
	private double xMin, xMax, yMin, yMax;

	private FadeTransition fadeTransition;
	private String labelStatusStyle;

	private Zoomer zoomer;

	private ChangeListener<SeriesParameters> seriesComboboxChangeListener;

	@FXML
	CheckBox checkboxGenerateSine;
	@FXML
	CheckBox checkboxGenerateParabolic;
	@FXML
	CheckBox checkboxGenerateExponential;

	@FXML
	CheckBox checkboxXSmooth;
	@FXML
	CheckBox checkboxYSmooth;
	@FXML
	CheckBox checkboxDisplayMarkers;
	@FXML
	CheckBox checkboxXLogScale;
	@FXML
	CheckBox checkboxYLogScale;

	@FXML
	ComboBox<FitTypeEnum> comboboxFitType;
	@FXML
	ComboBox<Marker> comboboxMarkerStyle;
	@FXML
	ComboBox<PlotTypeEnum> comboboxPlotType;
	@FXML
	ComboBox<SeriesParameters> comboboxSeries;
	@FXML
	ComboBox<LineStyle> comboboxLineStyle;

	@FXML
	ComboBox<ErrorStyle> comboboxErrorBarType;
	@FXML
	ComboBox<ErrorBarValueTypeEnum> comboboxYErrorBarValueType;

	@FXML
	TextField textfieldFileName;
	@FXML
	TextField textfieldCssStyle;
	@FXML
	TextField textfieldSeriesName;
	@FXML
	TextField textfieldXAxisLabel;
	@FXML
	TextField textfieldYAxisLabel;
	@FXML
	TextField textfieldXAxisUnits;
	@FXML
	TextField textfieldYAxisUnits;

	@FXML
	Spinner<Double> spinnerYErrorBarValue;
	@FXML
	Spinner<Double> spinnerDeltaXSize;
	@FXML
	Spinner<Double> spinnerMarkerSize;
	@FXML
	Spinner<Integer> spinnerXSmoothWindowSize;
	@FXML
	Spinner<Integer> spinnerYSmoothWindowSize;
	@FXML
	Spinner<Integer> spinnerFitDegree;
	@FXML
	Spinner<Integer> spinnerXColumn;
	@FXML
	Spinner<Integer> spinnerYColumn;
	@FXML
	Spinner<Integer> spinnerFirstDataRow;

	// for fitted line
	@FXML
	Spinner<Double> spinnerMarkerSizeFit;
	@FXML
	CheckBox checkboxDisplayMarkersFit;
	@FXML
	ComboBox<LineStyle> comboboxLineStyleFit;
	@FXML
	ComboBox<Marker> comboboxMarkerStyleFit;
	@FXML
	TextField textfieldCssStyleFit;

	@FXML
	Button buttonBrowseFiles;

	@FXML
	Button buttonPlot;

	@FXML
	Button buttonCropData;

	@FXML
	private Accordion accordionParameters;

	@FXML
	private TitledPane titledPaneDataSource;

	@FXML
	private TitledPane titledPaneSeriesParameters;

	@FXML
	private TitledPane titledPaneSeries;

	@FXML
	private BorderPane mainBorderPane;

	@FXML
	private Label labelStatus;

	public void reset() {

		seriesParametersList.clear();
		SeriesParameters seriesParameters = new SeriesParameters("series 1");
		seriesParameters.getxDataParameters().setDataColumn(1);
		seriesParameters.getyDataParameters().setDataColumn(2);

		seriesParametersList.add(seriesParameters);
		comboboxSeries.setItems(seriesParametersList);

		comboboxSeries.setValue(seriesParameters);
		comboboxFitType.setValue(FitTypeEnum.values()[0]);
		comboboxPlotType.setValue(PlotTypeEnum.values()[0]);
		comboboxLineStyle.setValue(LineStyle.NORMAL);
		comboboxLineStyleFit.setValue(LineStyle.NORMAL);
		comboboxMarkerStyle.setValue(DefaultMarker.CIRCLE);
		comboboxMarkerStyleFit.setValue(DefaultMarker.RECTANGLE1);
		comboboxErrorBarType.setValue(ErrorStyle.NONE);
		comboboxYErrorBarValueType.setValue(ErrorBarValueTypeEnum.NONE);
		bindParameters(seriesParameters);
	}

	@FXML
	public void initialize() {
		xAxis = new DefaultNumericAxis();
		yAxis = new DefaultNumericAxis();

		chart = new XYChart(xAxis, yAxis);
		zoomer = new Zoomer();
		chart.getStylesheets().add("chartstyle.css");

		chart.getPlugins().add(zoomer);
		chart.getPlugins().add(new Panner());
		chart.getPlugins().add(new EditAxis());
		chart.getPlugins().add(new DataPointTooltip());
		chart.getPlugins().add(new TableViewer());
		chart.getPlugins().add(new EditDataSet());

		SeriesParameters seriesParameters = new SeriesParameters("series 1");
		seriesParameters.getxDataParameters().setDataColumn(1);
		seriesParameters.getyDataParameters().setDataColumn(2);

		seriesParametersList.add(seriesParameters);
		comboboxSeries.setItems(seriesParametersList);

		comboboxFitType.setItems(FXCollections.observableList(List.of(FitTypeEnum.values())));
		comboboxPlotType.setItems(FXCollections.observableList(List.of(PlotTypeEnum.values())));
		comboboxLineStyle.setItems(FXCollections.observableList(List.of(LineStyle.values())));
		comboboxLineStyleFit.setItems(FXCollections.observableList(List.of(LineStyle.values())));
		comboboxMarkerStyle.setItems(FXCollections.observableList(List.of(DefaultMarker.values())));
		comboboxMarkerStyleFit.setItems(FXCollections.observableList(List.of(DefaultMarker.values())));
		comboboxErrorBarType.setItems(FXCollections.observableList(List.of(ErrorStyle.values())));
		comboboxYErrorBarValueType.setItems(FXCollections.observableArrayList(List.of(ErrorBarValueTypeEnum.values())));

		comboboxSeries.setValue(seriesParameters);
		comboboxFitType.setValue(FitTypeEnum.values()[0]);
		comboboxPlotType.setValue(PlotTypeEnum.values()[0]);
		comboboxLineStyle.setValue(LineStyle.NORMAL);
		comboboxLineStyleFit.setValue(LineStyle.NORMAL);
		comboboxMarkerStyle.setValue(DefaultMarker.CIRCLE);
		comboboxMarkerStyleFit.setValue(DefaultMarker.RECTANGLE1);
		comboboxErrorBarType.setValue(ErrorStyle.NONE);
		comboboxYErrorBarValueType.setValue(ErrorBarValueTypeEnum.NONE);

		xAxis.setUnit("units");
		yAxis.setUnit("units");

		SpinnerValueFactory<Integer> spinnerFitDegreeValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(
				0, 4, 1);
		SpinnerValueFactory<Integer> spinnerXSmoothValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(3,
				999999, 1);
		SpinnerValueFactory<Integer> spinnerYSmoothValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(3,
				999999, 1);
		SpinnerValueFactory<Double> spinnerDeltaXSizeValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(2,
				999999, 1);
		SpinnerValueFactory<Integer> spinnerXColumnValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,
				100, 1);
		SpinnerValueFactory<Integer> spinnerYColumnValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,
				100, 1);
		SpinnerValueFactory<Integer> spinnerFirstDataRowValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(
				1, 100, 1, 1);

		SpinnerValueFactory<Double> spinnerXScaleFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0,
				Double.MAX_VALUE, 1);
		SpinnerValueFactory<Double> spinnerYScaleFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0,
				Double.MAX_VALUE, 1);
		SpinnerValueFactory<Double> spinnerYErrorBarValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0,
				Double.MAX_VALUE, 1);
		SpinnerValueFactory<Double> spinnerMarkerSizeValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0,
				100, 2, 0.5);
		SpinnerValueFactory<Double> spinnerMarkerSizeValueFactoryFit = new SpinnerValueFactory.DoubleSpinnerValueFactory(
				0, 100, 2, 0.5);
		SpinnerValueFactory<Double> spinnerLineWidthValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0,
				100, 2, 0.5);

		StringConverter<Double> doubleConverter = new StringConverter<Double>() {
			private final DecimalFormat df = new DecimalFormat("0.######E0");

			@Override
			public String toString(Double object) {
				if (object == null) {
					return "";
				}
				return df.format(object);
			}

			@Override
			public Double fromString(String string) {
				try {
					if (string == null) {
						return null;
					}
					string = string.trim();
					if (string.length() < 1) {
						return null;
					}
					return df.parse(string).doubleValue();
				} catch (ParseException ex) {
					throw new RuntimeException(ex);
				}
			}
		};
		spinnerXScaleFactory.setConverter(doubleConverter);
		spinnerYScaleFactory.setConverter(doubleConverter);

		spinnerXSmoothWindowSize.setValueFactory(spinnerXSmoothValueFactory);
		spinnerYSmoothWindowSize.setValueFactory(spinnerYSmoothValueFactory);
		spinnerDeltaXSize.setValueFactory(spinnerDeltaXSizeValueFactory);
		spinnerXColumn.setValueFactory(spinnerXColumnValueFactory);
		spinnerYColumn.setValueFactory(spinnerYColumnValueFactory);
		spinnerMarkerSize.setValueFactory(spinnerMarkerSizeValueFactory);
		spinnerMarkerSizeFit.setValueFactory(spinnerMarkerSizeValueFactoryFit);
		spinnerFitDegree.setValueFactory(spinnerFitDegreeValueFactory);
		spinnerYErrorBarValue.setValueFactory(spinnerYErrorBarValueFactory);
		spinnerFirstDataRow.setValueFactory(spinnerFirstDataRowValueFactory);

		setAxisFields(seriesParameters);

		seriesComboboxChangeListener = new ChangeListener<SeriesParameters>() {

			@Override
			public void changed(ObservableValue<? extends SeriesParameters> observable, SeriesParameters oldValue,
					SeriesParameters newValue) {
				if (oldValue != null) {
					logger.info("unbinding old series " + oldValue.getName() + " " + oldValue.hashCode());
					unbindParameters(oldValue);

				}
				if (newValue != null) {
					logger.info("binding new series " + newValue.getName() + " " + newValue.hashCode());
					bindParameters(newValue);
					setTitledPaneSeriesParametersName();
				}
			}
		};

		comboboxSeries.valueProperty().addListener(seriesComboboxChangeListener);

		spinnerXColumn.valueProperty().addListener((ch, old, selection) -> {
			logger.info("old " + old + " new " + selection + " ch is " + ch);
			refreshChart();
		});
		spinnerYColumn.valueProperty().addListener((ch, old, selection) -> {
			logger.info("ch: " + ch + " old: " + old + " selection: " + selection);
			refreshChart();
		});
		textfieldXAxisLabel.textProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		textfieldYAxisLabel.textProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		checkboxXLogScale.selectedProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		checkboxYLogScale.selectedProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		checkboxXSmooth.selectedProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});
		checkboxYSmooth.selectedProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});
		spinnerXSmoothWindowSize.valueProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});
		spinnerYSmoothWindowSize.valueProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});

		comboboxYErrorBarValueType.valueProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});
		spinnerYErrorBarValue.valueProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});

		comboboxLineStyle.valueProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		comboboxLineStyleFit.valueProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		comboboxMarkerStyle.valueProperty().addListener((ch, old, selection) -> {
			getCurrentSeries().getDataRenderer().setMarker(selection);

			refreshChart();
		});
		comboboxMarkerStyleFit.valueProperty().addListener((ch, old, selection) -> {
			getCurrentSeries().getFitRenderer().setMarker(selection);
			refreshChart();
		});

		spinnerFitDegree.valueProperty().addListener((ch, old, selected) -> {
			try {
				plot();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			refreshChart();
		});

		checkboxDisplayMarkers.selectedProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});
		checkboxDisplayMarkersFit.selectedProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});

		spinnerMarkerSize.valueProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});
		spinnerMarkerSizeFit.valueProperty().addListener((ch, old, selected) -> {
			refreshChart();
		});

		comboboxErrorBarType.valueProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});

		fitTypeChangeListener = new ChangeListener<FitTypeEnum>() {
			@Override
			public void changed(ObservableValue<? extends FitTypeEnum> observable, FitTypeEnum oldValue,
					FitTypeEnum newValue) {
				try {
					plot();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				refreshChart();
			}
		};

		spinnerFitDegree.valueFactoryProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		spinnerDeltaXSize.valueProperty().addListener((ch, old, selection) -> {
			refreshChart();
		});
		comboboxPlotType.valueProperty().addListener((ch, old, selection) -> {
			refreshChart();
			zoomer.zoomOrigin();
		});
		textfieldCssStyle.textProperty().addListener((ch, old, selection) -> {
			getCurrentSeries().getDataRenderer().setCssStyle(selection);
			refreshChart();
		});
		textfieldCssStyleFit.textProperty().addListener((ch, old, selection) -> {
			getCurrentSeries().getFitRenderer().setCssStyle(selection);
			refreshChart();
		});

		bindParameters(seriesParameters);

		textfieldXAxisLabel.textProperty().bindBidirectional(xAxis.nameProperty());
		textfieldYAxisLabel.textProperty().bindBidirectional(yAxis.nameProperty());
		checkboxXLogScale.selectedProperty().bindBidirectional(xAxis.logAxisProperty());
		checkboxYLogScale.selectedProperty().bindBidirectional(yAxis.logAxisProperty());
		textfieldXAxisUnits.textProperty().bindBidirectional(xAxis.unitProperty());
		textfieldYAxisUnits.textProperty().bindBidirectional(yAxis.unitProperty());

		accordionParameters.setExpandedPane(titledPaneSeriesParameters);

		labelStatusStyle = labelStatus.getStyle();
	}

	public void textfieldSeriesNameEnterPressed(ActionEvent ae) {
		chart.requestLayout();
		getCurrentSeries().setName(textfieldSeriesName.getText());
		List<DataSet> dataSetList = new ArrayList<>();
		List<Renderer> rendererList = new ArrayList<>();
		for (SeriesParameters sp : seriesParametersList) {
			dataSetList.add(sp.getDataRenderer().getDataSet());
			rendererList.add(sp.getDataRenderer().getErrorRenderer());
			if (!sp.getFitType().equals(FitTypeEnum.NONE)) {
				dataSetList.add(sp.getFitRenderer().getDataSet());
				rendererList.add(sp.getFitRenderer().getErrorRenderer());
			}
		}

		setTitledPaneSeriesParametersName();
		chart.getLegend().updateLegend(dataSetList, rendererList);
	}

	protected void dumpSeriesList() {
		for (SeriesParameters sp : seriesParametersList) {
			logger.info(sp.getName());
			logger.info(sp.getDescriptionString());
		}
		logger.info("================= end of series list");
	}

	private DataCheckEnum checkData() {
		if (dataSource == null) {
			return DataCheckEnum.EMPTY;
		}

		if (dataSource.getColumn(spinnerXColumn.getValue() - 1).size() != dataSource
				.getColumn(spinnerYColumn.getValue() - 1).size()) {
			return DataCheckEnum.XYMISMATCH;
		}

		if (comboboxYErrorBarValueType.getValue().equals(ErrorBarValueTypeEnum.COLUMN)
				&& dataSource.getColumn(spinnerYErrorBarValue.getValue().intValue() - 1).size() != dataSource
						.getColumn(spinnerYColumn.getValue() - 1).size()) {
			return DataCheckEnum.ERRORBARMISMATCH;
		}

		return DataCheckEnum.OK;
	}

	private void setStatusMessage(String msg) {
		labelStatus.setText(msg);
		labelStatus.setTextFill(Color.RED);
		labelStatus.setStyle("-fx-background-color: black");
		fadeTransition = new FadeTransition(Duration.seconds(3), labelStatus);
		fadeTransition.setFromValue(1.0);
		fadeTransition.setToValue(0.0);
		fadeTransition.setCycleCount(Animation.INDEFINITE);
		fadeTransition.play();

	}

	private void displayDataCheckResult(DataCheckEnum dce) {
		logger.info("switching on result " + dce);

		switch (dce) {
		case EMPTY:
			break;
		case OK:
			break;
		case ERRORBARMISMATCH:
			String s;
			s = "Number of error bar values does not match number of y values.";
			s += " Check that your column number is correct for the error bars.";
			logger.info(s);
			setStatusMessage("errorbar column has wrong length");

			break;
		case XYMISMATCH:
			setStatusMessage("x and y columns are different lengths");

			String ss = "Number of x values does not match number of y values.";
			ss += " Check that your column numbers are correct";
			logger.info(ss);
			break;
		default:
			break;
		}
	}

	private DataCheckEnum assignDataToSeries() {
		DataCheckEnum dce = checkData();
		switch (dce) {
		case EMPTY:
			return dce;
		case ERRORBARMISMATCH:
			return dce;
		case OK:
			break;
		case XYMISMATCH:
			return dce;
		}

		SeriesParameters sp = getCurrentSeries();

		logger.info("dataSource is " + dataSource);
		double x[] = dataSource.getColumnAsPrimitive(spinnerXColumn.getValue() - 1);
		double y[] = dataSource.getColumnAsPrimitive(spinnerYColumn.getValue() - 1);

		DataFuncs1D.boxcar(checkboxXSmooth.isSelected(), x, spinnerXSmoothWindowSize.getValue());
		DataFuncs1D.boxcar(checkboxYSmooth.isSelected(), y, spinnerYSmoothWindowSize.getValue());

		double yerrNeg[] = dataFuncs1D.assignError(comboboxYErrorBarValueType.getValue(), y,
				spinnerYErrorBarValue.getValue() - 1, spinnerYColumn.getValue() - 1);
		double yerrPos[] = dataFuncs1D.assignError(comboboxYErrorBarValueType.getValue(), y,
				spinnerYErrorBarValue.getValue() - 1, spinnerYColumn.getValue() - 1);

		DoubleErrorDataSet dataSet = new DoubleErrorDataSet("data");

		logger.info("x: " + x.length + " y:" + y.length);

		dataSet.set(x, y, yerrNeg, yerrPos);

		logger.info("plot type is " + comboboxPlotType.getValue());
		String titleString = dataSource.getNameForChartTitle();

		switch (comboboxPlotType.getValue()) {
		case DATA:
			sp.getDataRenderer().getDataSet().set(dataSet);
			break;
		case FIRST_DERIVATIVE:
			logger.info("first deriv, x size is " + x.length);
			double slope[][] = DataFuncs2D.slope(x, y, spinnerDeltaXSize.getValue().intValue());
			DoubleDataSet deriv = new DoubleDataSet("first derivative");
			for (int i = 0; i < slope.length; ++i) {
				deriv.add(slope[i][0], slope[i][1]);
			}
			logger.info("first deriv, slope x size is " + slope.length);
			sp.getDataRenderer().getDataSet().clearData();
			sp.getDataRenderer().getDataSet().set(deriv);
			logger.info("first deriv, data set size is " + sp.getDataRenderer().getDataSet().getDataCount());

			/*
			sp.getDataRenderer().getDataSet().clearData();
			DataSet derivative = DataSetMath.derivativeFunction(dataSet);
			sp.getDataRenderer().getDataSet().set(derivative);
			*/
			break;
		case SECOND_DERIVATIVE:
			/*
			slope = DataFuncs2D.slope(x, y, spinnerDeltaXSize.getValue().intValue());
			double[][] secondSlope = DataFuncs2D.slope(slope, spinnerDeltaXSize.getValue().intValue());
			
			DoubleDataSet secondDeriv = new DoubleDataSet("second derivative");
			for (int i = 0; i < slope.length; ++i) {
				secondDeriv.add(secondSlope[i][0], secondSlope[i][1]);
			}
			sp.getDataRenderer().getDataSet().set(secondDeriv);
			*/
			break;
		default:
			break;

		}

		switch (comboboxFitType.getValue()) {
		case POLYNOMIAL:
			NonLinearRegressionFitter fitter = new NonLinearRegressionFitter(x, y);
			sp.setFitter(fitter);
			logger.info("data fitter: max x = " + DataFuncs1D.max(x));
			logger.info("data fitter: min x = " + DataFuncs1D.min(x));
			logger.info("data fitter: max y = " + DataFuncs1D.max(y));
			logger.info("data fitter: min y = " + DataFuncs1D.min(y));

			double[] coeffs = new double[spinnerFitDegree.getValue() + 1];
			double[] step = new double[spinnerFitDegree.getValue() + 1];
			for (int i = 0; i < coeffs.length; ++i) {
				coeffs[i] = 1;
				step[i] = 0.05;
			}
			PolyFunction func = new PolyFunction("poly", spinnerFitDegree.getValue());
			fitter.simplex(func, coeffs, step);

			double[] fittedParameter = fitter.getBestEstimates();

			func.setParameterValues(fittedParameter);
			double[] yFit = func.getValues(x);
			DoubleDataSet fit = new DoubleDataSet("fit");
			fit.set(x, yFit);
			sp.getFitRenderer().getDataSet().set(fit);

			titleString += "\n" + sp.getFitString();
			chart.setTitle(titleString);
			break;
		case NONE:
			break;
		case SINE:
			fitter = new NonLinearRegressionFitter(x, y);
			sp.setFitter(fitter);
			coeffs = new double[4];
			step = new double[4];
			for (int i = 0; i < coeffs.length; ++i) {
				coeffs[i] = 1;
				step[i] = 0.1;
			}
			SineFunction sinefunc = new SineFunction();
			fitter.simplex(sinefunc, coeffs, step);

			fittedParameter = fitter.getBestEstimates();

			sinefunc.setParameterValues(fittedParameter);
			yFit = sinefunc.getValues(x);
			fit = new DoubleDataSet("fit");
			fit.set(x, yFit);
			sp.getFitRenderer().getDataSet().set(fit);
			break;
		case EXPONENTIAL:
			fitter = new NonLinearRegressionFitter(x, y);
			sp.setFitter(fitter);
			coeffs = new double[4];
			step = new double[4];
			for (int i = 0; i < coeffs.length; ++i) {
				coeffs[i] = 1;
				step[i] = 0.1;
			}
			ExponentialFunction expFunc = new ExponentialFunction();
			fitter.simplex(expFunc, coeffs, step);

			fittedParameter = fitter.getBestEstimates();

			expFunc.setParameterValues(fittedParameter);
			yFit = expFunc.getValues(x);
			fit = new DoubleDataSet("fit");
			fit.set(x, yFit);
			sp.getFitRenderer().getDataSet().set(fit);
			break;
		}

		return DataCheckEnum.OK;
	}

	@FXML
	void quit() {
		Platform.exit();
	}

	private SeriesParameters getCurrentSeries() {
		return comboboxSeries.getValue();
	}

	@FXML
	void addSeries() {
		String name = "series " + (seriesParametersList.size() + 1);
		logger.info("adding new " + name);
		unbindParameters(getCurrentSeries());

		SeriesParameters seriesParameters = new SeriesParameters(name);
		seriesParametersList.add(seriesParameters);
		comboboxSeries.valueProperty().removeListener(seriesComboboxChangeListener);
		comboboxSeries.setValue(seriesParameters);
		comboboxSeries.valueProperty().addListener(seriesComboboxChangeListener);

		bindParameters(seriesParameters);
		setTitledPaneSeriesParametersName();

		setStatusMessage("set x and y column numbers and click plot");
	}

	private void setTitledPaneSeriesParametersName() {

		titledPaneSeriesParameters.setText("series: " + getCurrentSeries().getName());
	}

	@FXML
	void cropData() throws Exception {

		xMin = xAxis.getMin();
		xMax = xAxis.getMax();
		yMin = yAxis.getMin();
		yMax = yAxis.getMax();

		((DataReaderCsv) dataSource).crop(seriesParametersList, xMin, xMax, yMin, yMax);

		assignDataToSeries();
	}

	@FXML
	void deleteSeries() {
		logger.info("delete series");
		dumpSeriesList();

		logger.info("chart has " + chart.getRenderers().size() + " renderers");
		logger.info("chart has " + chart.getDatasets().size() + " datasets");

		logger.info("x axis runs from " + xAxis.getMin() + " to " + xAxis.getMax());

		SeriesParameters sp = getCurrentSeries();
		/*
		for (int i = 0; i < sp.getDataSet().getDataCount(); i++) {
			double x = sp.getDataSet().get(0, i);
			double y = sp.getDataSet().get(1, i);
		
			logger.info("(" + x + ", " + y);
		}
		*/
		logger.info("fit type is " + sp.getFitType());
		for (int i = 0; i < sp.getFitRenderer().getDataSet().getDataCount(); i++) {
			double x = sp.getFitRenderer().getDataSet().get(0, i);
			double y = sp.getFitRenderer().getDataSet().get(1, i);

			logger.info("(" + x + ", " + y);
		}

	}

	// set form axis data to axis parms
	void setAxisFields(SeriesParameters seriesParameters) {
		logger.info("setting axis field for series " + seriesParameters.getName() + " " + seriesParameters.hashCode());
		textfieldSeriesName.setText(seriesParameters.getName());

		ErrorDataSetRenderer renderer = seriesParameters.getDataRenderer().getErrorRenderer();
		ErrorDataSetRenderer fitRenderer = seriesParameters.getDataRenderer().getErrorRenderer();

		comboboxLineStyle.setValue(renderer.polyLineStyleProperty().getValue());
		comboboxLineStyleFit.setValue(fitRenderer.polyLineStyleProperty().getValue());
		comboboxMarkerStyle.setValue(renderer.getMarker());
		comboboxMarkerStyleFit.setValue(fitRenderer.getMarker());
		comboboxErrorBarType.setValue(renderer.errorStyleProperty().getValue());
		comboboxFitType.setValue(seriesParameters.getFitTypeProperty().getValue());
		comboboxPlotType.setValue(seriesParameters.getPlotTypeProperty().getValue());

		checkboxDisplayMarkers.setSelected(renderer.isDrawMarker());
		checkboxDisplayMarkersFit.setSelected(fitRenderer.isDrawMarker());

		textfieldCssStyle.setText(seriesParameters.getDataRenderer().getCssStyle());
		textfieldCssStyleFit.setText(seriesParameters.getFitRenderer().getCssStyle());

		spinnerFitDegree.getValueFactory().setValue(seriesParameters.getFitDegree());
		spinnerDeltaXSize.getValueFactory().setValue(seriesParameters.getDxSize());
		spinnerMarkerSize.getValueFactory().setValue(renderer.getMarkerSize());
		spinnerMarkerSizeFit.getValueFactory().setValue(fitRenderer.getMarkerSize());

		checkboxXSmooth.setSelected(seriesParameters.getxDataParameters().isSmooth());

		spinnerXColumn.getValueFactory().setValue(seriesParameters.getxDataParameters().getDataColumn());
		spinnerXSmoothWindowSize.getValueFactory().setValue(seriesParameters.getxDataParameters().getSmoothingWindow());

		checkboxYSmooth.setSelected(seriesParameters.getyDataParameters().isSmooth());

		spinnerYColumn.getValueFactory().setValue(seriesParameters.getyDataParameters().getDataColumn());
		spinnerYSmoothWindowSize.getValueFactory().setValue(seriesParameters.getyDataParameters().getSmoothingWindow());
		spinnerYErrorBarValue.getValueFactory().setValue(seriesParameters.getyDataParameters().getErrorBarValue());

		comboboxYErrorBarValueType.valueProperty()
				.setValue(seriesParameters.getyDataParameters().getErrorBarValueType());

	}

	// these are bound and change with different series
	void bindParameters(SeriesParameters seriesParameters) {

		setAxisFields(seriesParameters);

		textfieldSeriesName.textProperty().bindBidirectional(seriesParameters.getNameProperty());

		SeriesRenderer dataRenderer = seriesParameters.getDataRenderer();
		SeriesRenderer fitRenderer = seriesParameters.getFitRenderer();

		comboboxLineStyle.valueProperty().bindBidirectional(dataRenderer.getErrorRenderer().polyLineStyleProperty());
		comboboxLineStyleFit.valueProperty().bindBidirectional(fitRenderer.getErrorRenderer().polyLineStyleProperty());
		comboboxErrorBarType.valueProperty().bindBidirectional(dataRenderer.getErrorRenderer().errorStyleProperty());
		comboboxFitType.valueProperty().bindBidirectional(seriesParameters.getFitTypeProperty());
		comboboxPlotType.valueProperty().bindBidirectional(seriesParameters.getPlotTypeProperty());

		checkboxDisplayMarkers.selectedProperty()
				.bindBidirectional(dataRenderer.getErrorRenderer().drawMarkerProperty());
		checkboxDisplayMarkersFit.selectedProperty()
				.bindBidirectional(fitRenderer.getErrorRenderer().drawMarkerProperty());

		textfieldCssStyle.textProperty().bindBidirectional(dataRenderer.getCssStyleProperty());
		textfieldCssStyleFit.textProperty().bindBidirectional(fitRenderer.getCssStyleProperty());

		spinnerFitDegree.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getFitDegreePropertyObject());
		spinnerDeltaXSize.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getDeltaXSizePropertyObject());
		spinnerMarkerSize.getValueFactory().valueProperty()
				.bindBidirectional(dataRenderer.getMarkerSizePropertyObject());
		spinnerMarkerSizeFit.getValueFactory().valueProperty()
				.bindBidirectional(fitRenderer.getMarkerSizePropertyObject());

		checkboxXSmooth.selectedProperty().bindBidirectional(seriesParameters.getxDataParameters().getSmoothProperty());

		spinnerXColumn.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getxDataParameters().getDataColumnPropertyObject());
		spinnerXSmoothWindowSize.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getxDataParameters().getSmoothingWindowSizePropertyObject());

		checkboxYSmooth.selectedProperty().bindBidirectional(seriesParameters.getyDataParameters().getSmoothProperty());

		spinnerYColumn.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getyDataParameters().getDataColumnPropertyObject());
		spinnerYSmoothWindowSize.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getyDataParameters().getSmoothingWindowSizePropertyObject());
		spinnerYErrorBarValue.getValueFactory().valueProperty()
				.bindBidirectional(seriesParameters.getyDataParameters().getErrorBarValuePropertyObject());

		comboboxYErrorBarValueType.valueProperty()
				.bindBidirectional(seriesParameters.getyDataParameters().getErrorBarValueTypeProperty());

		seriesParameters.getFitTypeProperty().addListener(fitTypeChangeListener);

	}

	void unbindParameters(SeriesParameters seriesParameters) {
		textfieldSeriesName.textProperty().unbindBidirectional(seriesParameters.getNameProperty());

		SeriesRenderer dataRenderer = seriesParameters.getDataRenderer();
		SeriesRenderer fitRenderer = seriesParameters.getFitRenderer();

		comboboxLineStyle.valueProperty().unbindBidirectional(dataRenderer.getErrorRenderer().polyLineStyleProperty());
		comboboxLineStyleFit.valueProperty()
				.unbindBidirectional(fitRenderer.getErrorRenderer().polyLineStyleProperty());
		comboboxErrorBarType.valueProperty().unbindBidirectional(dataRenderer.getErrorRenderer().errorStyleProperty());
		comboboxFitType.valueProperty().unbindBidirectional(seriesParameters.getFitTypeProperty());
		comboboxPlotType.valueProperty().unbindBidirectional(seriesParameters.getPlotTypeProperty());
		comboboxYErrorBarValueType.valueProperty()
				.unbindBidirectional(seriesParameters.getxDataParameters().getErrorBarValueTypeProperty());

		checkboxDisplayMarkers.selectedProperty()
				.unbindBidirectional(dataRenderer.getErrorRenderer().drawMarkerProperty());
		checkboxDisplayMarkersFit.selectedProperty()
				.unbindBidirectional(fitRenderer.getErrorRenderer().drawMarkerProperty());

		textfieldCssStyle.textProperty().unbindBidirectional(dataRenderer.getCssStyleProperty());
		textfieldCssStyleFit.textProperty().unbindBidirectional(fitRenderer.getCssStyleProperty());

		spinnerFitDegree.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getFitDegreePropertyObject());
		spinnerDeltaXSize.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getDeltaXSizePropertyObject());
		spinnerMarkerSize.getValueFactory().valueProperty()
				.unbindBidirectional(dataRenderer.getMarkerSizePropertyObject());
		spinnerMarkerSizeFit.getValueFactory().valueProperty()
				.unbindBidirectional(fitRenderer.getMarkerSizePropertyObject());

		spinnerXColumn.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getxDataParameters().getDataColumnPropertyObject());
		checkboxXSmooth.selectedProperty()
				.unbindBidirectional(seriesParameters.getxDataParameters().getSmoothProperty());
		spinnerXSmoothWindowSize.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getxDataParameters().getSmoothingWindowSizePropertyObject());
		comboboxYErrorBarValueType.valueProperty()
				.unbindBidirectional(seriesParameters.getxDataParameters().getErrorBarValueTypeProperty());

		spinnerYColumn.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getyDataParameters().getDataColumnPropertyObject());
		checkboxYSmooth.selectedProperty()
				.unbindBidirectional(seriesParameters.getyDataParameters().getSmoothProperty());
		spinnerYSmoothWindowSize.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getyDataParameters().getSmoothingWindowSizePropertyObject());
		spinnerYErrorBarValue.getValueFactory().valueProperty()
				.unbindBidirectional(seriesParameters.getyDataParameters().getErrorBarValuePropertyObject());
		comboboxYErrorBarValueType.valueProperty()
				.unbindBidirectional(seriesParameters.getyDataParameters().getErrorBarValueTypeProperty());

		seriesParameters.getFitTypeProperty().removeListener(fitTypeChangeListener);

	}

	@FXML
	void seriesChanged() {
		SeriesParameters currentSeriesParameters = getCurrentSeries();
		logger.info("selected series " + currentSeriesParameters.getName());
		logger.info(currentSeriesParameters.getDescriptionString());

	}

	@FXML
	private void showEditor() {
		logger.info("showing editor");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(DFPlotController.class.getResource("/ca/tfuller/dfplot/gui/dfploteditor.fxml"));
		try {
			BorderPane bp = (BorderPane) loader.load();
			mainBorderPane.setCenter(bp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	@FXML
	public void loadFile() {
		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				loadFile2();
				return null;
			}
		};
		task.stateProperty().addListener(new ChangeListener<Worker.State>() {
			@Override
			public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue,
					Worker.State newValue) {
				logger.info("state changed from " + oldValue + " to " + newValue);
				if (newValue != Worker.State.RUNNING) {
					primaryStage.getScene().setCursor(Cursor.DEFAULT);
				} else if (newValue == Worker.State.RUNNING) {
					primaryStage.getScene().setCursor(Cursor.WAIT);
				}
			}
		});
		Thread th = new Thread(task);
		th.start();
	}

	public void loadFile2() {

		int firstDataRow = spinnerFirstDataRow.getValue();
		if (textfieldFileName.getText().startsWith("https")) {
			dataSource = new DataReaderGoogleSheet(textfieldFileName.getText(), firstDataRow);
			logger.info("dataSource is " + dataSource);
			dataFuncs1D = new DataFuncs1D(dataSource);
			textfieldFileName.setText(dataSource.getFilename());

			chart.setTitle(dataSource.getNameForChartTitle());
		} else {
			File file = new File(textfieldFileName.getText());

			dataSource = new DataReaderCsv(file.getAbsolutePath(), firstDataRow);
			dataFuncs1D = new DataFuncs1D(dataSource);
		}
	}

	@FXML
	public void browseFiles() {
		logger.info("browse files");
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open csv file");
		File file = fileChooser.showOpenDialog(null);

		if (file != null) {
			textfieldFileName.setText(file.getAbsolutePath());
		} else {
			textfieldFileName.setText("");
		}
		chart.setTitle(dataSource.getNameForChartTitle());
	}

	@FXML
	private void showDataTable() {

		Parent root;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DFPlot.class.getResource("/ca/tfuller/dfplot/gui/dftable.fxml"));
			root = (AnchorPane) loader.load();

			Stage stage = new Stage();
			stage.setTitle("Data");
			stage.setScene(new Scene(root, 450, 450));
			stage.show();

			DFTableViewController dftvc = (DFTableViewController) loader.getController();
			dftvc.setDataSource(dataSource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void showFitStats() {
		try {

			if (fitStatsStage == null) {
				Parent fitStatsRoot = null;

				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(DFPlot.class.getResource("/ca/tfuller/dfplot/gui/dffitstats.fxml"));
				fitStatsRoot = (AnchorPane) loader.load();
				fitStatsController = (DFFitStatsController) loader.getController();

				fitStatsStage = new Stage();
				fitStatsStage.setTitle("Fit statistics");
				fitStatsStage.setScene(new Scene(fitStatsRoot, 700, 500));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fitStatsStage.show();
		fitStatsController.showFitStats(seriesParametersList);
	}

	private void refreshFitStats() {
		if (fitStatsStage != null && fitStatsStage.isShowing()) {
			fitStatsController.showFitStats(seriesParametersList);
		}
	}

	public void refreshChart() {
		DataCheckEnum dce = assignDataToSeries();
		// reset status label
		if (fadeTransition != null) {
			logger.info("stopping face transition");
			fadeTransition.stop();
		}
		labelStatus.setText("");
		labelStatus.setStyle(labelStatusStyle);

		logger.info("assignDataToSeries result is " + dce);

		switch (dce) {
		case EMPTY:
			break;
		case ERRORBARMISMATCH:
		case XYMISMATCH:
			displayDataCheckResult(dce);
			break;
		case OK:
			chart.requestLayout();
			refreshFitStats();
			break;
		default:
			break;
		}
	}

	@FXML
	public void plot() throws Exception {
		logger.info("plot");
		assignDataToSeries();

		chart.getStylesheets().remove("chartstyle.css");
		chart.getStylesheets().add("chartstyle.css");

		chart.getGridRenderer().getHorizontalMinorGrid().setStroke(Color.BLUE);
		chart.getGridRenderer().getHorizontalMinorGrid().setStrokeWidth(2.0d);

		List<DataSet> dataSets = new ArrayList<>();
		List<Renderer> renderers = new ArrayList<>();
		for (SeriesParameters sp : seriesParametersList) {
			dataSets.add(sp.getDataRenderer().getDataSet());
			switch (sp.getFitType()) {
			case NONE:
				break;
			case POLYNOMIAL:
				dataSets.add(sp.getFitRenderer().getDataSet());
				renderers.add(sp.getFitRenderer().getErrorRenderer());
				break;
			case SINE:
				dataSets.add(sp.getFitRenderer().getDataSet());
				renderers.add(sp.getFitRenderer().getErrorRenderer());
				break;
			case EXPONENTIAL:
				dataSets.add(sp.getFitRenderer().getDataSet());
				renderers.add(sp.getFitRenderer().getErrorRenderer());
				break;

			}
			renderers.add(sp.getDataRenderer().getErrorRenderer());
		}

		chart.getRenderers().setAll(renderers);

		// New window (Stage)
		if (plotWindow == null) {
			plotWindow = new Stage();
			StackPane plotLayout = new StackPane();
			plotLayout.getChildren().add(chart);

			Scene plotScene = new Scene(plotLayout, 800, 800);
			plotWindow.setTitle("DFPlot");
			plotWindow.setScene(plotScene);

		}
		plotWindow.show();
	}

}
