package ca.tfuller.dfplot.gui;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.BorderPane;

public class DFPlotEditorController {
	protected static final Logger logger = LogManager.getLogger(DFPlotEditorController.class);

	@FXML
	private BorderPane borderPaneEditor;

	@FXML
	private Button buttonAddColumn;

	@FXML
	private Button buttonDeleteColumn;

	List<TableColumn> tableColumns = new ArrayList<TableColumn>();

	private SpreadsheetView spreadSheetView;

	@FXML
	public void save() {
		logger.info("save");
	}

	@FXML
	public void addColumn() {

	}

	@FXML
	public void deleteColumn() {

	}

	@FXML
	public void initialize() {
		int rowCount = 15;
		int columnCount = 10;
		GridBase grid = new GridBase(rowCount, columnCount);

		ObservableList<ObservableList<SpreadsheetCell>> rows = FXCollections.observableArrayList();
		for (int row = 0; row < grid.getRowCount(); ++row) {
			final ObservableList<SpreadsheetCell> list = FXCollections.observableArrayList();
			for (int column = 0; column < grid.getColumnCount(); ++column) {
				list.add(SpreadsheetCellType.STRING.createCell(row, column, 1, 1, "value"));
			}
			rows.add(list);
		}
		grid.setRows(rows);

		SpreadsheetView spv = new SpreadsheetView(grid);
		borderPaneEditor.setCenter(spv);
	}
}
