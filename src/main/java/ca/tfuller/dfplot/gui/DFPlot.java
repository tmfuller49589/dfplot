package ca.tfuller.dfplot.gui;

import java.io.IOException;
import java.lang.Runtime.Version;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class DFPlot extends Application {
	private Stage primaryStage;

	private BorderPane rootLayout;
	private DFPlotController dfPlotController;
	protected static final Logger logger = LogManager.getLogger(DFPlot.class);
	private static Scene scene;

	public DFPlot() {

	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DFPlot.class.getResource("/ca/tfuller/dfplot/gui/dfplot.fxml"));
			rootLayout = (BorderPane) loader.load();

			dfPlotController = (DFPlotController) loader.getController();
			// Show the scene containing the root layout.
			scene = new Scene(rootLayout);

			primaryStage.setScene(scene);
			primaryStage.show();
			dfPlotController.setPrimaryStage(primaryStage);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage priStage) {
		try {

			Version version = java.lang.Runtime.version();
			System.out.println("Java Version = " + version);

			this.primaryStage = priStage;
			this.primaryStage.setTitle("DFPlot");

			initRootLayout();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
