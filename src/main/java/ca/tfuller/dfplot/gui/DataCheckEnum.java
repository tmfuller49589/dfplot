package ca.tfuller.dfplot.gui;

public enum DataCheckEnum {
	OK, XYMISMATCH, ERRORBARMISMATCH, EMPTY;
}
