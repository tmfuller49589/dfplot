package ca.tfuller.dfplot.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfplot.datautils.DataSource;
import ca.tfuller.dfplot.datautils.FitTypeEnum;
import ca.tfuller.dfplot.datautils.SeriesParameters;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

public class DFFitStatsController {
	protected static final Logger logger = LogManager.getLogger(DFFitStatsController.class);
	private static DataSource dataSource;

	@FXML
	AnchorPane anchorPaneMain;

	@FXML
	TextArea textArea;

	public static DataSource getDataSource() {
		return dataSource;
	}

	@FXML
	public void initialize() {

	}

	public void showFitStats(ObservableList<SeriesParameters> seriesParametersList) {
		String s = "";
		for (SeriesParameters sp : seriesParametersList) {
			s += sp.getName() + "\n\n";
			if (sp.getFitType().equals(FitTypeEnum.NONE)) {
				s += "No fit performed\n\n";
			} else {
				s += sp.getFitString() + "\n\n";
				s += sp.getFitStatsString() + "\n\n================================================\n\n";
			}
		}

		logger.info(s);
		textArea.setStyle("-fx-font-family: 'monospaced';");
		textArea.setText(s);
	}

}
