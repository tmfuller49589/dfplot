package ca.tfuller.dfplot.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import ca.tfuller.dfplot.datautils.DataSource;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class DFTableViewController {
	protected static final Logger logger = LogManager.getLogger(DFTableViewController.class);
	private static DataSource dataSource;

	@FXML
	AnchorPane anchorPaneMain;

	public static DataSource getDataSource() {
		return dataSource;
	}

	@FXML
	public void initialize() {
		logger.info("here i am");
	}

	public void setDataSource(DataSource dataSource) {
		DFTableViewController.dataSource = dataSource;

		int rowCount = dataSource.getMaxRowCount();
		int columnCount = dataSource.getColumnCount();

		GridBase grid = new GridBase(rowCount, columnCount);

		ObservableList<ObservableList<SpreadsheetCell>> rows = FXCollections.observableArrayList();

		for (int row = 0; row < grid.getRowCount(); ++row) {
			final ObservableList<SpreadsheetCell> list = FXCollections.observableArrayList();
			for (int column = 0; column < grid.getColumnCount(); ++column) {
				if (row < dataSource.getRowCount(column)) {
					Double val = dataSource.getColumn(column).get(row);
					if (val != null) {
						list.add(SpreadsheetCellType.STRING.createCell(row, column, 1, 1, val.toString()));
					} else {
						list.add(SpreadsheetCellType.STRING.createCell(row, column, 1, 1, ""));
					}
				} else {
					list.add(SpreadsheetCellType.STRING.createCell(row, column, 1, 1, ""));
				}
			}
			rows.add(list);
		}
		grid.setRows(rows);

		SpreadsheetView spv = new SpreadsheetView(grid);
		anchorPaneMain.getChildren().add(spv);

		AnchorPane.setTopAnchor(spv, 0d);
		AnchorPane.setBottomAnchor(spv, 0d);
		AnchorPane.setRightAnchor(spv, 0d);
		AnchorPane.setLeftAnchor(spv, 0d);
	}

}
